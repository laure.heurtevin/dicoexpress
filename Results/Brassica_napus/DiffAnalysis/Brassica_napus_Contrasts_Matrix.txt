Contrasts	(Intercept)	TissueRoot	TreatmentSi	ReplicateR2	ReplicateR3	TissueRoot:TreatmentSi
[MatureLeaf-Root]	0	-1	0	0	0	-0.5
[NoSi-Si]	0	0	-1	0	0	-0.5
[NoSi_MatureLeaf-NoSi_Root]	0	-1	0	0	0	0
[Si_MatureLeaf-Si_Root]	0	-1	0	0	0	-1
[MatureLeaf_NoSi-MatureLeaf_Si]	0	0	-1	0	0	0
[Root_NoSi-Root_Si]	0	0	-1	0	0	-1
[MatureLeaf_NoSi-MatureLeaf_Si]-[Root_NoSi-Root_Si]	0	0	0	0	0	-1
