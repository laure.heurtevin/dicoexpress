###### Create Venn diagram ######

Venn_IntersectUnion <- function(Data_Directory, Results_Directory, Project_Name, Title,  Groups, Operation)
{    
  
    ## subdirectory "Project"
    if (!I(Project_Name %in% dir(Results_Directory))){
        cat("This project does not exist\n")
        break
    }
    ## subdirectory
    subdirectory <- "Venn_Intersection_Union"
    path=paste0(Results_Directory,"/",Project_Name,"/",subdirectory)
    if (!I(subdirectory %in% dir(paste0(Results_Directory,"/",Project_Name)))) dir.create(path, showWarnings=FALSE)
    if (!I(Title %in% dir(paste0(Results_Directory,"/",Project_Name,"/",subdirectory)))) dir.create(paste0(path,"/",Title), showWarnings=FALSE)
    
    ## Load Compare table
    if (I(paste0(Project_Name,"_Compare_table.txt") %in% dir(path)))
    {
        Compare <- read.csv2(paste0(path,"/",Project_Name,"_Compare_table.txt"), header=TRUE, sep="\t",check.names=FALSE,row.names=1)
    }
    else
    {
        Compare <- read.csv2(paste0(Results_Directory,"/",Project_Name,"/DiffAnalysis/",Project_Name,"_Compare_table.txt"), header=TRUE, sep="\t",check.names=FALSE,row.names=1)
    }
  
    ## check if the groups are really studied contrasts
    if (sum(Groups%in%colnames(Compare[,Groups]))!=length(Groups))
    {
        cat("Groups is bad defined. The constrast(s) \n")
        cat(Groups[!Groups%in%colnames(Compare)])
        cat("do(es) not exist \n")
        break
    }
    
    ## Union List
    if (Operation=="Union"){
        newCol <- ncol(Compare)+1
        Compare[,newCol]=apply(Compare[,Groups],1,function(a) 1*(sum(a)!=0))
        colnames(Compare)[newCol]<- paste0(Title,"_Union")
        IdGene <- rownames(Compare)[which(Compare[,newCol]==1)]
        Compare_subset <- Compare[IdGene,Groups]  
        Summary_Union <- data.frame("Gene_ID"=IdGene,DE_Group=apply(Compare_subset,1,function(a) paste(LETTERS[which(a!=0)],collapse='')))
        
        ## Add Gene_Name
        if(!is.null(Annotation))
            Summary_Union <- merge(Annotation, Summary_Union, by="Gene_ID",all.y=TRUE)
        
        fileout <- paste0(path,"/",Title,"/",Project_Name,"_",Title,"_Union_List.txt")
        write.table(IdGene,fileout,row.names=FALSE,col.names=FALSE, sep="\t", quote = FALSE)
        
        fileout <- paste0(path,"/",Title,"/",Project_Name,"_",Title,"_Union_Summary_Table.txt")
        write.table(Summary_Union,fileout,row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
        
        ## Add a column to the compare table
        fileout <-  paste0(path,"/",Project_Name,"_Compare_table.txt")
        Compare <-  tibble::rownames_to_column(Compare, "Gene_ID")
        write.table(Compare,fileout,row.names=FALSE,col.names=TRUE,sep="\t",quote=FALSE)
    }
  
  ## Intersection List
    if (Operation == "Intersection")
    {
        newCol <- ncol(Compare)+1
        Compare[,newCol]=apply(Compare[,Groups],1,function(a) 1*(sum(a)==length(Groups)))
        colnames(Compare)[newCol]<- paste0(Title,"_Intersection")
        IdGene <- rownames(Compare)[which(Compare[,newCol]==1)]
         IdGene <- data.frame("Gene_ID"=IdGene)

        ## Add Gene_Name
        if(!is.null(Annotation))
            IdGene <- merge(Annotation, IdGene, by="Gene_ID",all.y=TRUE)
        
        ## Intersection List 
        fileout <- paste0(path,"/",Title,"/",Project_Name,"_",Title,"_Intersection_List.txt")
        write.table(IdGene,fileout,row.names=FALSE,col.names=FALSE, sep="\t", quote = FALSE)
        
        ## Add a column to the compare table
        fileout <-  paste0(path,"/",Project_Name,"_Compare_table.txt")
        Compare <-  tibble::rownames_to_column(Compare, "Gene_ID")
        write.table(Compare,fileout,row.names=FALSE,col.names=TRUE,sep="\t",quote=FALSE)
    }
    
    if(Operation != "Union" && Operation != "Intersection")
    {
        cat("The Operation is misinformed. The possible values for the Operation argument are Union or Intersection")
    }
    
    ## check of the number of groups that are compared
    if (length(Groups) >=6)
    {
        cat("Cannot plot Venn diagram for more than 5 sets \n")
        break
    }
    
    ## Create Venn diagram
    pdf(paste0(path,"/",Title,"/",Project_Name,"_",Title,"_Venn_Diagram.pdf"))
    Comparison_table <- vennCounts(Compare[,Groups])
    Venn <- vennDiagram(Comparison_table, circle.col = brewer.pal(length(Groups),"Set2"), cex=1, names=LETTERS[1:length(Groups)])
    for (a in 1:length(Groups))
        title(paste0(LETTERS[a], " = ",Groups[a]), line = -22-a, adj=0.1 ,cex.main = 0.6)
    dev.off()
}







